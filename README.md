# Links, ideas and miscellaneous...

https://gitlab.com/riquebueno/wikisql-reader/boards

pip install --proxy http://chave:senha@inet-sys.gnet.petrobras.com.br:804 -U (se quiser fazer update) PACOTE

git config --global http.proxy http://chave:senha@inet-sys.gnet.petrobras.com.br:804

https://rogerdudler.github.io/git-guide/

datasets: http://nlpprogress.com/english/semantic_parsing.html

https://devguide.python.org/coverage/

https://developer.ibm.com/recipes/tutorials/testing-and-code-coverage-with-python/

# GIT TIPS from https://rogerdudler.github.io/git-guide/

- Local Rep
- Tree 1: Working Directory
- git add *
- Tree 2: Index
- git commit -m "comentários das alterações"
- Tree 3: HEAD
- git push origin master

- to update your local repository to the newest commit, execute: git pull

- About GitLab board https://about.gitlab.com/2018/08/02/4-ways-to-use-gitlab-issue-boards/

# TENSOR FLOW TIPS

# JENKINS TIPS

# SONAR TIPS

# UNIX TIPS

# NEXUS TIPS

# DOCKER TIPS
- https://training.play-with-docker.com/
- https://training.play-with-docker.com/beginner-linux/ (30 minutes from any computer)
- https://www.linux.com/blog/paypal-reduces-costs-10x-open-source-ci

# KUBERNETS

# JUPYTER
- How create a Jupyter by command?

# CLOUD
- Single Page Application (SPA)
- Serverless + Lambda + 
- AMAZON: Study list of services (AMAZON COMPREHEND)
-
#BLOCKCHAIN
- 

# OTHERS
- How to unzip a .tar.bz2 file using 7 zip in windows?
-- "C:\Program Files\7-Zip\7z.exe" x data.tar.bz2
-- "C:\Program Files\7-Zip\7z.exe" x data.tar
- How to set Windows PATH from command line? - setx path "%path%;c:\dir1\dir2"
- Apache Camel
- https://xebialabs.com/periodic-table-of-devops-tools/
- Devops term first time: https://www.youtube.com/watch?v=LdOe18KhtT4&t=55s